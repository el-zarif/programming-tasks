
### How do I run the code ? ###

To install scrapy framework:
```
#!bash
pip3 install scrapy
```
To run The web scraper code and save the results to csv:
```
#!bash
scrapy runspider scrappy.py --nolog -o result.csv
```

To run the Grid search code:
```
#!bash
python3 grid-search.py
```

# Dependecies
```
Python 3.4.3
Scrapy 1.5.0
```

# Acknowledgment 
The scraper work was guided by this tutorial:
https://www.digitalocean.com/community/tutorials/how-to-crawl-a-web-page-with-scrapy-and-python-3
