
'''
    the method performs DFS search on the board starting the points i, j to find the string
    :param board: 2d list board 
    :param int i: vertical index
    :param int j: horizontal index
    :param str string: string to be found
    :return: true if the string is found
'''
def dfs_board(board, i, j, string):
    # the base cases either index out of bounds or different chars
    if i < 0 or i >= len(board) or j < 0 or j >= len(board[0]) or string[0] != board[i][j]:
        return False
    # last case 1 char left from string and equal to the board char
    if len(string) == 1 and board[i][j] == string[0]:
        return True
    # save the char in tmp so it wont be revisited and assign back after visiting other nodes
    tmp = board[i][j]
    board[i][j] = '!'
    # try all different traversals 
    result = dfs_board(board, i+1, j, string[1:]) or dfs_board(board, i-1, j, string[1:]) or dfs_board(board, i, j+1, string[1:]) or dfs_board(board, i, j-1, string[1:])
    board[i][j] = tmp
    return result

'''
    the method invokes the DFS search from each slot in the board
    :param board: 2d list board 
    :param str string: string to be found
    :return: true if the string is found
'''
def search(board, string):
    for i in range(len(board)):
        for j in range(len(board[0])):
            if dfs_board(board, i, j, string):
                return True
    return False

if __name__ == "__main__":
    board = [['A', 'B', 'C', 'E'],
             ['S', 'F', 'C', 'S'],
             ['A', 'D', 'E', 'E']]

    print('ABCCED -> {}'.format(search(board, 'ABCCED')))
    print('SEE -> {}'.format(search(board, 'SEE')))
    print('ABCB-> {}'.format(search(board, 'ABCB')))

    