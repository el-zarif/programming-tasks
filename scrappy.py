import scrapy
import re


class JiraSpider(scrapy.Spider):
    name = 'jira_spider'
    start_urls = ['https://issues.apache.org/jira/browse/CAMEL-10597']

    '''
		the method parses the content of the webpage based on the CSS ids and html structue
		:param response: the web response scrapy object
		:return: parsed data dictionary
	'''
    def parse(self, response):
        keys = []
        values = []
        # parsing the issue details (type, status, etc)
        for item in response.css('#issuedetails li'):
            keys.append(item.css('.name ::text').extract_first())
            values.append(self.filterText(item.css('.value').extract_first()))

        # parsing the people details (reporter, assignee)
        for item in response.css('#peopledetails'):
        	roles = item.css('dl dt::text').extract()
        	for role in roles:
        		keys.append(role)
        	names = item.css('dl dd').extract()
        	for name in names:
           		values.append(self.filterText(name))

        # parsing the date info (created, updated, resolved)
        for item in response.css('#datesmodule'):
            date_desc = item.css('dl dt::text').extract()
            for desc in date_desc:
            	keys.append(desc)
            date_values = item.css('dl dd').extract()
            for val in date_values:
            	values.append(self.filterText(val))

        # parsing the description and the comments
        keys.append('Description')
        values.append(response.css('#description-val ::text').extract())
        keys.append('Comments')
        values.append(response.css('#issue_actions_container ::text').extract())

        if len(keys) != len(values):
            print('Error: different number of keys and values')
            return

        dictionary = {}
        for i in range(len(keys)):
            dictionary[keys[i]] = values[i]
        print(dictionary)
        yield dictionary


    '''
		the method cleans up the parsed string from the tags
		:param string: the html tag to be parsed
		:return array: the array of the parsed values from the tag
	'''
    def filterText(self, string):
        string = string.strip()
        found = re.findall(r'>\s*[^\s]+\s*[^\s]+\s*<', string)
        new_found = []
        for result in found:
            lindex = result.index('>')
            rindex = result.index('<')
            result = result[lindex + 1: rindex]
            result = result.strip()
            if len(result) > 0:
                new_found.append(result)
        return new_found
